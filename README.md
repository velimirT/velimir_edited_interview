<h1>Installation Instructions</h1>
<ol>
  <li>Run `yarn install` to install the project dependencies</li>
  <li>Run `yarn start` to start the project</li>
  <li>Visit http://localhost:3000/ to see the project running</li>
</ol>
<h2>* most of the code is located in ./src/app/</h2>
<h2>Screenshot</h2>
<img width="914" alt="React Boilerplate Meets CRA" src="Firefox_Screenshot_2023-10-24T04-36-41.527Z.png" />
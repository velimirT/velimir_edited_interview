import React, { memo } from 'react';
import styled from 'styled-components/macro';

type InputProps = React.DetailedHTMLProps<
  React.InputHTMLAttributes<HTMLInputElement>,
  HTMLInputElement
>;

interface Props extends InputProps {
  id: string;
  className?: string;
  type: string;
  error: string;
}

export const Input = memo(
  ({ id, className, type, error, ...restOf }: Props) => {
    return (
      <Wrapper className={className}>
        <input type={type} id={id} {...restOf} />
        { error !== '' && <p className="error">{error}</p> }
      </Wrapper>
    );
  },
);

const Wrapper = styled.div`
  input{
    width: 365px;
    height: 50px;
    margin: 0px;
    margin-bottom: 18px;
    padding: 17px;
    background: rgba(240, 237, 255, 0.80);
    border: none;
    border-radius: 16px;
    color: #787878;
    font-family: Poppins;
    font-size: 12px; 
  }
  .error{
    color: red;
  }
`;

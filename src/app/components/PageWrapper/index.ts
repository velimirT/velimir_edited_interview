import styled from 'styled-components/macro';

export const PageWrapper = styled.div`
  display: flex;
  margin: 0 auto;
  padding: 20px;
  box-sizing: border-box;
  justify-content: center;
  align-items: center;
`;

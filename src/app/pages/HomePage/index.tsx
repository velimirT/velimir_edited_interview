import * as React from 'react';
import styled from 'styled-components/macro';
import { useSelector, useDispatch } from 'react-redux';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import { Input } from '../../components/Input';
import { PageWrapper } from 'app/components/PageWrapper';
import { useHomepageSlice } from './slice';
import {
  selectLogged,
  selectError,
  selectUsername,
  selectRememberMe,
} from './slice/selectors';
import { messages } from './messages';

export function HomePage() {
  const [username, setUsername] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [usernameError, setUsernameError] = React.useState('');
  const [passwordError, setPasswordError] = React.useState('');
  const { actions } = useHomepageSlice();
  const dispatch = useDispatch();
  const logged = useSelector(selectLogged);
  const error = useSelector(selectError);
  const savedUsername = useSelector(selectUsername);
  const rememberMe = useSelector(selectRememberMe);
  const { t } = useTranslation();

  const onSubmitForm = (evt?: React.FormEvent<HTMLFormElement>) => {
    /* istanbul ignore next  */
    if (evt !== undefined && evt.preventDefault) {
      let currentUsername = (rememberMe && savedUsername) ? savedUsername : username;
      evt.preventDefault();
      if(validate(currentUsername, password)){
        console.log('Form is valid!', evt);
        dispatch(actions.login({username: currentUsername, password: password}));
        setUsername('');
        setPassword('');
      }else{
        console.log('Form is INvalid');
      }
    }
  };

  const validate = (username: string, password: string) => {
    let result = true;
    if( !String(username)
    .toLowerCase()
    .match(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    )){
      setUsernameError('Please enter a valid username');
      result = false;
    }else{
      setUsernameError('');
    }
    if(password.length < 5 || !String(password)
      .toLowerCase()
      .match(
        /[a-zA-Z]+[\d+]/
      )){
      setPasswordError('Please enter a valid password');
      result = false;
    }else{
      setPasswordError('');
    }
    return result;
  }

  const onLogOut = () => {
    dispatch(actions.logout());
  }
  
  const changeRemembeMe = () => {
    dispatch(actions.rememberMe());
  }
  
  return (
    <>
      <Helmet>
        <title>Home Page</title>
        <meta
          name="description"
          content="A React Boilerplate application homepage"
        />
      </Helmet>
      <PageWrapper>
        {
          !logged && (
          <LoginFormWrapper>
            <h2>{t(messages.loginFormTitle())}</h2>      
            <form onSubmit={onSubmitForm}>
              {
                (savedUsername !== null && rememberMe === true) && (
                  <p>{savedUsername}</p>
                )
              }
              {
                (sessionStorage.getItem('username') === null || rememberMe === false) && (
                  <Input type="text" placeholder="Username" error={usernameError} id="username" onChange={(e)=>{setUsername(e.target.value)}}/>
                )
              }
             <Input type="password" placeholder="Password" error={passwordError} id="password" onChange={(e)=>{setPassword(e.target.value)}}/>
              {
                error && <ErrorText>{error}</ErrorText>
              }
              <RememberMe>
                <input type="checkbox" id="rememberMe" defaultChecked={rememberMe} onChange={changeRemembeMe} />
                <label htmlFor="rememberMe">Remember Me</label>
              </RememberMe>
              <LoginButton type="submit">Login Now</LoginButton>
            </form>
          </LoginFormWrapper>
          )
        }
        {
          logged && (
            <LoginFormWrapper>
              <h2>Hello, {savedUsername ? savedUsername : username}!</h2>
              <LoginButton onClick={onLogOut}>Log Out</LoginButton>
            </LoginFormWrapper>
          )
        }
        
      </PageWrapper>
    </>
  );
}

const ErrorText = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  min-height: 50px;
  color: red;
`;

const LoginFormWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 600px;
  height: 600px;
  align-items: center;
  justify-content: center;
  padding: 78px 45px;
  border-radius: 24px; 
  background-color: #fff;
  box-sizing: border-box;
  justify-content: start;
  & h2{
    font-size: 21px;
    margin-top: 0;
    margin-bottom: 70px;
  }
`;

const RememberMe = styled.div`
  margin-bottom: 50px;
  & label{
    font-size: 16px;
    line-height: 28px;
  }
  & input{
    margin: 8px;
    width: 16px;
    height: 16px;
  }
`;

const LoginButton = styled.button`
  display: flex;
  margin: 0 auto;
  padding: 17px 30px;
  text-align: center;
  font-size: 12px;
  font-weight: 700;
  color: #fff;
  background: linear-gradient(100deg, #9181F4 -5.85%, #5038ED 109.55%);
  border-radius: 16px;
  border: none;
  box-shadow: 0px 8px 21px 0px rgba(0, 0, 0, 0.16);
  cursor: pointer;
`;
import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit'; // Importing from `utils` makes them more type-safe ✅
import { HomepageState } from './types';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { homepageSaga } from './saga';

// The initial state of the Homepage
export const initialState: HomepageState = {
  loading: false,
  logged: false,
  loginSuccess: false,
  error: '',
  rememberMe: sessionStorage.getItem('rememberMe') !== null,
  username: sessionStorage.getItem('username') === null ? '' : sessionStorage.getItem('username'),
};
console.log('Saga', homepageSaga);
const slice = createSlice({
  name: 'homepage',
  initialState,
  reducers: {
    login(state, action: any) {
      // Here we say lets change the username in my homepage state when changeUsername actions fires
      // Type-safe: It will expect `string` when firing the action. ✅
      state.loading = true;
      state.username = action.payload.username;
      state.error = '';
    },
    loginSuccess(state, action: PayloadAction<string>) {
      // Here we say lets change the username in my homepage state when changeUsername actions fires
      // Type-safe: It will expect `string` when firing the action. ✅
      state.logged = true;
      state.loading = false;
      state.error = '';
      state.username !== null && sessionStorage.setItem('username', state.username);
    },
    loginError(state, action: PayloadAction<string>) {
      state.error = action.payload;
      state.loading = false;
      state.logged = false;
    },
    logout(state) {
      state.error = '';
      state.loading = false;
      state.logged = false;
    },
    rememberMe(state) {
      state.rememberMe = !state.rememberMe;
      state.rememberMe ? sessionStorage.setItem('rememberMe', 'true') : sessionStorage.removeItem('rememberMe');
      ;
    }
  },
});

/**
 * `actions` will be used to trigger change in the state from where ever you want
 */
export const { actions: homepageActions } = slice;

// ... code from above

/**
 * Let's turn this into a hook style usage. This will inject the slice to redux store and return actions in case you want to use in the component
 */
export const useHomepageSlice = () => {
  useInjectReducer({ key: slice.name, reducer: slice.reducer });
  useInjectSaga({ key: slice.name, saga: homepageSaga });
  return { actions: slice.actions };
};
import { call, put, select, takeLatest, delay } from 'redux-saga/effects';
import { request } from 'utils/request';
import { homepageActions as actions } from '.';

export function* login(action){
  const {username, password} = action.payload;
  if(username === "hello@edited.com" && password === "hello123"){
    sessionStorage.setItem("rememberMe", username);
    yield put(actions.loginSuccess('true'));
    console.log('sessionStorage', sessionStorage.getItem('rememberMe'));
  }else{
    yield put(actions.loginError('Username or password doesn\'t match our records'))
  }
  // const requestURL = `https://api.github.com/users/${username}/repos?type=all&sort=updated`;

  // const logged: boolean = yield call(request, requestURL);
}

// Root saga
export function* homepageSaga() {
  // if necessary, start multiple sagas at once with `all`
  yield takeLatest(actions.login.type, login);
}
import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from '.';

// First select the relevant part from the state
const selectDomain = (state: RootState) => state.homepage || initialState;

export const selectLogged = createSelector(
  [selectDomain],
  HomepageState => HomepageState.logged,
);

export const selectError = createSelector(
  [selectDomain],
  HomepageState => HomepageState.error,
);

export const selectUsername = createSelector(
  [selectDomain],
  HomepageState => HomepageState.username,
);

export const selectRememberMe = createSelector(
  [selectDomain],
  HomepageState => HomepageState.rememberMe,
);
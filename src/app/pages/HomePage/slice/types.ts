export interface HomepageState {
  loading: boolean;
  logged: boolean;
  loginSuccess: boolean;
  error: string;
  rememberMe: boolean;
  username: string | null;
}